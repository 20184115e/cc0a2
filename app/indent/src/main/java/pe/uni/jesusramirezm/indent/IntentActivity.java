package pe.uni.jesusramirezm.indent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class IntentActivity extends AppCompatActivity {

    EditText editText, editNumber;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);

        editText = findViewById(R.id.edit_text);
        editNumber = findViewById(R.id.edit_number);
        button = findViewById(R.id.button);

        button.setOnClickListener(v -> {

            // Convertimos las entradas a cadenas
            String sText = editText.getText().toString();
            String sNumber = editNumber.getText().toString();

            // Segunda actividad
            Intent intent = new Intent( IntentActivity.this, SecondActivity.class);

            // Le damos el dato a la segunda actividad
            intent.putExtra("TEXT", sText);

            // El número lo pasamos a entero
            if (!sNumber.equals("")) {
                int number = Integer.parseInt(sNumber);
                // Le damos el dato a la segunda actividad
                intent.putExtra("NUMBER", number);
            }

            startActivity(intent);

            // Si quisiéramos terminarlo, en este caso será posible volver así que no.
            //finish();
        });
    }
}