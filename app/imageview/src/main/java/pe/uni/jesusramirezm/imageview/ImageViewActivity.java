package pe.uni.jesusramirezm.imageview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageViewActivity extends AppCompatActivity {

    ImageView imageView;
    Button button;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);


        imageView = findViewById(R.id.image_view);
        button = findViewById(R.id.button);
        textView = findViewById(R.id.text_view);

        button.setOnClickListener(v -> {

                if (textView.getText().toString().equals("Brillo de atardecer")) {
                imageView.setImageResource(R.drawable.twilight);
                textView.setText(R.string.twilight);
                button.setText(R.string.text_button_2);

                }

                else {
                    imageView.setImageResource(R.drawable.sunset);
                    textView.setText(R.string.sunset);
                    button.setText(R.string.text_button_1);

                }
        });
    }

}