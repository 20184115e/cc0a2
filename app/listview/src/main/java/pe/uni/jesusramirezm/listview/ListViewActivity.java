package pe.uni.jesusramirezm.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListViewActivity extends AppCompatActivity {

    ListView listView;
    String[] choices;
    ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        listView = findViewById(R.id.list_view);
        choices = getResources().getStringArray(R.array.choices);

        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, choices);

        listView.setAdapter(arrayAdapter);

        // Al hacer click en uno de los ítems
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String choice = parent.getItemAtPosition(position).toString();

                Toast.makeText(getApplicationContext(), String.format(getResources().getString(R.string.toast_msg), choice), Toast.LENGTH_LONG)
                        .show();

            }
        });


    }
}