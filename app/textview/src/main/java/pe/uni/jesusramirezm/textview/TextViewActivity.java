package pe.uni.jesusramirezm.textview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.graphics.Color;

public class TextViewActivity extends AppCompatActivity {

    TextView text_view_1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view);

        text_view_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                text_view_1 = findViewById(R.id.text_view_1);
                text_view_1.setText(R.string.text_view_en);
                text_view_1.setBackgroundColor(Color.RED);

            }
        });

    }
}