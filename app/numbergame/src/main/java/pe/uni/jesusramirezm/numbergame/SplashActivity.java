package pe.uni.jesusramirezm.numbergame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {

    ImageView imageViewSplash;
    TextView textViewSplash;

    Animation animationImage, animationText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Enlazando animaciones
        imageViewSplash = findViewById(R.id.image_view_splash);
        textViewSplash = findViewById(R.id.text_view_splash);

        animationImage = AnimationUtils.loadAnimation(this, R.anim.image_animation);
        animationText = AnimationUtils.loadAnimation(this, R.anim.text_animation);

        imageViewSplash.setAnimation(animationImage);
        textViewSplash.setAnimation(animationText);

        // Timer

        /* El número de milisegundos en el futuro desde la llamada a start()
        hasta que finaliza la cuenta regresiva y se llama a onFinish() */

        /* El intervalo a lo largo del camino para recibir callbacks de llamada onTick(long) */

        new CountDownTimer(5000, 1000) {

           @Override
           public void onTick(long millisUntilFinished) {

           }

           @Override
           public void onFinish() {

               // Intent para pasar a otra actividad. En este caso, Main.
               Intent intent = new Intent(SplashActivity.this, MainActivity.class);
               startActivity(intent);
               finish();
           }

       }.start();
    }

}