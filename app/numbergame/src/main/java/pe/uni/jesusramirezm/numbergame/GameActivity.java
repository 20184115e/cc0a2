package pe.uni.jesusramirezm.numbergame;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class GameActivity extends AppCompatActivity {

    // Para recuperar a través de ID
    TextView textViewLastAttempt, textViewRemainAttempt, textViewHint;
    EditText editTextGuessNumber;
    Button buttonFindGuessNumber;

    // Para reconocer la opción escogida en el GroupRadio
    Boolean twoDigits, threeDigits, fourDigits;

    // Aleatorio en Android Studio
    Random r = new Random();
    int random = 0;

    // Intentos restantes
    int remainAttempts = 10;

    // Inicializamos un arreglo para guardar los números de nuestros intentos
    ArrayList<Integer> guessesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // Recuperamos
        textViewLastAttempt = findViewById(R.id.text_view_last_attempt);
        textViewRemainAttempt = findViewById(R.id.text_view_remain_attempts);
        textViewHint = findViewById(R.id.text_view_hint);
        editTextGuessNumber = findViewById(R.id.edit_text_guess_number);
        buttonFindGuessNumber = findViewById(R.id.button_find_guess_number);

        // Recuperamos de la actividad anterior a través de un Intent.
        twoDigits = getIntent().getBooleanExtra("TWO" , false);
        threeDigits = getIntent().getBooleanExtra("THREE" , false);
        fourDigits = getIntent().getBooleanExtra("FOUR" , false);

        // Se puede escribir así para mayor entendimiento
        // Intent intent = getIntent();

        // Escogemos el intervalo dependiendo del booleano
        if (twoDigits) {
            //10 + [0, 90>
            random = 10 + r.nextInt(90);
        }
        if (threeDigits) {
            //100 + [0, 900>
            random = 100 + r.nextInt(900);
        }
        if (fourDigits) {
            //1000 + [0, 9000>
            random = 1000 + r.nextInt(9000);
        }

        // Al hacer click en el botón adivinar
        buttonFindGuessNumber.setOnClickListener(v -> {

            // Guardamos el número ingresado
            String sGuess = editTextGuessNumber.getText().toString();

            // Un mensaje de advertencia tipo Toast si no se ingresó ningún número
            if (sGuess.equals("")) {
                Toast.makeText(GameActivity.this, R.string.edit_text_msg, Toast.LENGTH_LONG).show();
                return;
            }

            // Reducimos el número de intentos
            remainAttempts--;

            // Convertimos el número ingresado a entero
            int iGuess = Integer.parseInt(sGuess);

            // Añadimos el número ingresado a la lista de intentos
            guessesList.add(iGuess);

            // Alteramos la visibilidad de los campos de texto para
            // visualizar número de intentos, etc.
            textViewLastAttempt.setVisibility(View.VISIBLE);
            textViewRemainAttempt.setVisibility(View.VISIBLE);
            textViewHint.setVisibility(View.VISIBLE);

            // Para concatenar cadenas en el XML
            Resources res = getResources();
            textViewLastAttempt.setText(String.format(res.getString(R.string.text_view_last_attempt), sGuess)); //Tu último intento fue #
            textViewRemainAttempt.setText(String.format(res.getString(R.string.text_view_remain_attempts), remainAttempts));

            // Win
            if (random == iGuess) {

                // Tipo de alerta Builder
                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                builder.setTitle("");
                builder.setCancelable(false);
                // Mensaje que mostraremos, el cual cuenta con 3 variables en el XML
                // Si bien el tercer parámetro no es un String, se convierte automáticamente
                builder.setMessage(String.format(res.getString(R.string.game_msg_win), random, (10-remainAttempts), guessesList));

                // Respuesta positiva
                builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        /* Usamos un Intent para volver a la actividad Main y volver
                           a jugar. */
                        Intent intent = new Intent(GameActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                // Respuesta negativa
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Proceso para cerrar la aplicación
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    }
                });

                builder.create().show();
            }

            // In process
            if (random < iGuess) {
                textViewHint.setText(R.string.text_view_hint_decrease);
            }
            if (random > iGuess) {
                textViewHint.setText(R.string.text_view_hint_increase);
            }

            // Lose
            if (remainAttempts == 0) {

                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                builder.setTitle("");
                builder.setCancelable(false);
                // Mensaje que mostraremos, el cual cuenta con 2 variables en el XML
                builder.setMessage(String.format(res.getString(R.string.game_msg_lose), random, guessesList));

                // Respuesta positiva
                builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        /* Usamos un Intent para volver a la actividad Main y volver
                           a jugar. */
                        Intent intent = new Intent(GameActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                // Respuesta negativa
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Proceso para cerrar la aplicación
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    }
                });

                builder.create().show();
            }

            editTextGuessNumber.setText("");
        });

    }
}