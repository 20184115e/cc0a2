package pe.uni.jesusramirezm.numbergame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    RadioButton radioButtonTwo, radioButtonThree, radioButtonFour;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButtonTwo = findViewById(R.id.radio_button_two);
        radioButtonThree = findViewById(R.id.radio_button_three);
        radioButtonFour = findViewById(R.id.radio_button_four);
        button = findViewById(R.id.button_start);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!radioButtonTwo.isChecked() && !radioButtonThree.isChecked() && !radioButtonFour.isChecked()) {

                    Snackbar.make(v, R.string.snack_bar_msg, Snackbar.LENGTH_LONG).show();
                    return ;
                }

                Intent intent = new Intent(MainActivity.this, GameActivity.class);

                if (radioButtonTwo.isChecked()) {
                    intent.putExtra("TWO", true);
                }

                if (radioButtonThree.isChecked()) {
                    intent.putExtra("THREE", true);
                }
                if (radioButtonFour.isChecked()) {
                    intent.putExtra("FOUR", true);
                }

                startActivity(intent);
                // Ya que podremos volver a esta actividad, no la terminamos

            }
        });
    }
}