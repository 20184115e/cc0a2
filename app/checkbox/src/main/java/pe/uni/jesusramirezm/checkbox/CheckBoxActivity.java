package pe.uni.jesusramirezm.checkbox;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.TextView;

public class CheckBoxActivity extends AppCompatActivity {

    TextView textView;
    CheckBox checkbox1;
    CheckBox checkbox2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box);

        textView = findViewById(R.id.text_view);
        checkbox1 = findViewById(R.id.checkbox_1);
        checkbox2 = findViewById(R.id.checkbox_2);

        checkbox1.setOnClickListener( v -> {

            if (checkbox1.isChecked()) {
                textView.setText(R.string.msg1);
                checkbox2.setChecked(false);
            } else {
                textView.setText(R.string.msg);
            }
        });

        checkbox2.setOnClickListener( v -> {

            if (checkbox2.isChecked()) {
                textView.setText(R.string.msg2);
                checkbox1.setChecked(false);

            } else {
                textView.setText(R.string.msg);
            }
        });


    }
}