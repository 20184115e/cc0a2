package pe.uni.jesusramirezm.togglebutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;

public class ToggleButton extends AppCompatActivity {

    ImageView imageView;
    CompoundButton toggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toggle_button);

        toggleButton = findViewById(R.id.toggleButton);
        imageView = findViewById(R.id.image_view_logo);


        toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if (isChecked) {

                imageView.setVisibility((View.INVISIBLE));
            } else {

                imageView.setVisibility(View.VISIBLE);

            }

        });
    }
}