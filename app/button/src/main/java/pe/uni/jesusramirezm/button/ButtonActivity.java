package pe.uni.jesusramirezm.button;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.graphics.Color;

public class ButtonActivity extends AppCompatActivity {

    Button button1;
    Button button2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);

        button1 = findViewById(R.id.button_1);
        button2 = findViewById(R.id.button_2);

        //Estándar de acciones a realizar al hacer click
        button1.setOnClickListener(v -> {

                button1.setBackgroundColor(Color.BLUE);
                button2.setVisibility(View.VISIBLE);
            });


    }
}