package pe.uni.jesusramirezm.sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class SharedPreferenceActivity extends AppCompatActivity {

    EditText editTextName;
    EditText editTextMessage;
    Button button;
    CheckBox checkBox;
    int counter = 0;

    String name, message;
    boolean isChecked;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);

        editTextName = findViewById(R.id.edit_text_name);
        editTextMessage = findViewById(R.id.edit_text_message);
        button = findViewById(R.id.button);
        checkBox = findViewById(R.id.check_box);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                counter++;
                button.setText(String.valueOf(counter));
            }
        });

        // Recuperamos la data
        retrieveData();
    }

    // FUNCIÓN POR DONDE LA APLICACIÓN SIEMPRE PASA
    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    // Ahora veremos como guardar estos datos, incluso si cerramos la aplicación.
    private void saveData() {

        // Instancia
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);

        // Datos a guardar
        name = editTextName.getText().toString();
        message = editTextMessage.getText().toString();
        isChecked = checkBox.isChecked();

        SharedPreferences.Editor editor = sharedPreferences.edit();

        // Llaves asociadas con las que recuperaremos los datos
        editor.putString("key name", name);
        editor.putString("key message", message);
        editor.putInt("key counter", counter);
        editor.putBoolean("key remember", isChecked);
        editor.apply();

        Toast.makeText(getApplicationContext(), "Tus datos están guardados", Toast.LENGTH_LONG)
                .show();

    }

    // Para recuperar los datos guardados
    private void retrieveData() {

        // Instancia
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);

        // Recuperando datos con las llaves
        name = sharedPreferences.getString("key name", null);
        message = sharedPreferences.getString("key message", null);
        counter = sharedPreferences.getInt("key counter", 0);
        isChecked = sharedPreferences.getBoolean("key remember", false);

        // Asignamos los datos a los componentes
        editTextName.setText(name);
        editTextMessage.setText(message);
        button.setText(String.valueOf(counter));
        checkBox.setChecked(isChecked);

    }
}