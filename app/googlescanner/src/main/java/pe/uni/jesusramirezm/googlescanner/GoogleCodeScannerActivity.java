package pe.uni.jesusramirezm.googlescanner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.common.MlKitException;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanner;
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning;

// Librería de un buen samaritano
import com.bosphere.filelogger.FL;

public class GoogleCodeScannerActivity extends AppCompatActivity {

    // Para los logs
    private static final String TAG = GoogleCodeScannerActivity.class.getSimpleName();

    TextView textViewScanner;
    Button buttonScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Librería de Logs de un buen samaritano
        FL.i(TAG,"onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_code_scanner);

        textViewScanner = findViewById(R.id.text_view_scanner);
        buttonScanner = findViewById(R.id.button_scanner);

        buttonScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GmsBarcodeScannerOptions options = new GmsBarcodeScannerOptions.Builder()
                        .setBarcodeFormats(
                                Barcode.FORMAT_QR_CODE
                        )
                        .build();

                GmsBarcodeScanner scanner = GmsBarcodeScanning.getClient(getApplicationContext(), options);

                scanner.startScan()
                        .addOnSuccessListener(new OnSuccessListener<Barcode>() {
                            @Override
                            public void onSuccess(Barcode barcode) {

                                FL.i("addOnSuccessListener");
                                // Mostramos en el textView
                                textViewScanner.setText(getSuccessfulMessage(barcode));

                            }
                        })

                        .addOnCanceledListener(new OnCanceledListener() {
                            @Override
                            public void onCanceled() {

                                FL.i("addOnCanceledListener");
                                textViewScanner.setText(R.string.cancelled_option);
                            }
                        })

                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                                FL.i(TAG,"Error: %s", e);
                                textViewScanner.setText(getFailureExceptionMessage( (MlKitException) e));


                            }
                        });
            }
        });
    }


    private String getSuccessfulMessage(Barcode barcode) {

        // Librería de Logs de un buen samaritano
        FL.i("getSuccessfulMessage");
        // String con parámetros
        return String.format(getResources().getString(R.string.barcode_result),
                barcode.getRawValue(),
                barcode.getFormat(),
                barcode.getValueType());
    }

    private String getFailureExceptionMessage(MlKitException e) {


        FL.i("getFailureExceptionMessage");
        switch (e.getErrorCode()) {

            case MlKitException.CODE_SCANNER_CANCELLED:
                return getString(R.string.error_code_scanner_cancelled);
            case MlKitException.UNKNOWN:
                return getString(R.string.error_code_unknown);
            default:
                return getString(R.string.error_default , e);
        }
    }
}